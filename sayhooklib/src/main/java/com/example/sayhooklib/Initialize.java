package com.example.sayhooklib;

import android.app.Application;
import android.util.Log;

import com.swift.sandhook.SandHook;
import com.swift.sandhook.SandHookConfig;
import com.swift.sandhook.lib.BuildConfig;
import com.swift.sandhook.wrapper.HookErrorException;

public class Initialize extends Application {

    /** in order to register initialize as application you need to add:
     *  android:name="com.example.sayhooklib.Initialize" to your manifest under application
     *  or to extend a class that already register as application**/

    private static final String TAG = "Initialize" ;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "onCreate: initialize hook " );
        SandHookConfig.DEBUG = BuildConfig.DEBUG;
        try {
            SandHook.addHookClass(ActivityHooker.class);
        } catch (HookErrorException e) {
            e.printStackTrace();
        }
    }

//    @Test
//    public boolean RegistrationSuccessful(String webViewUrl) {
//        RestAssured.baseURI ="https://someurl.com/foo";
//        RequestSpecification request = RestAssured.given();
//
//        JSONObject requestParams = new JSONObject();
//        requestParams.put("url",webViewUrl);// Cast
//
//        request.body(requestParams.toJSONString());
//        Response response = request.post("/blacklist");
//
//        String urlStatus = response.jsonPath().get("UrlStatus");
//             if (urlStatus.equals("malicious")){
//                 return true;
//             }
//        return false;
//    }
}

